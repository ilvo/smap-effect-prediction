.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _SMAPeffectindex:

SMAP effect-prediction
======================

| This is the manual for the effect-prediction component of the SMAP package.
| **SMAP effect-prediction** is designed to provide biological interpretation of the haplotype call tables created by **SMAP haplotype-window**.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   effect_scope_usage
   effect_feature_description
   effect_HIW
   effect_examples
   effect_faq
